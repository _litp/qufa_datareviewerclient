import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Grid, Paper, Input, InputLabel } from "@material-ui/core";
import { MetaSchemaTitle } from "./MetaSchemaTitle";
import logoImg from "assets/imgs/logos/api_conf.png";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLaptopCode, faSlidersH } from "@fortawesome/free-solid-svg-icons";
import { display } from "@material-ui/system";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    background: "rgb(242,243,247)",
    background:
      "linear-gradient(118deg, rgba(242,243,247,1) 0%, rgba(225,229,245,1) 100%)",
    borderLeft: "1px solid #95A3B6",
    padding: theme.spacing(2),
  },
  fontIcon: {
    color: "#95A3B6",
    fontSize: "1rem",
    marginRight: theme.spacing(1),
  },
  formWrap: {
    padding: `0 ${theme.spacing(2)}px`,
  },
  descForm: {
    padding: `${theme.spacing(1)}px`,
    "&.label": {
      display: "flex",
      alignItems: "center",
      justifyContent: "flex-end",
    },
  },
}));

export function MetaOptions(props) {
  const classes = useStyles();

  return (
    <div>
      <MetaSchemaTitle img={logoImg} title="API 설정" />

      <div className={classes.root}>
        <div mb={4}>
          <FontAwesomeIcon icon={faLaptopCode} className={classes.fontIcon} />
          API 상세 설명
        </div>

        <Paper>
          <Grid container spacing={0}>
            <Grid item className={`${classes.descForm} label`} xs={3}>
              <InputLabel htmlFor="resource-name">데이터명</InputLabel>
            </Grid>
            <Grid item className={classes.descForm} xs={9}>
              <Input
                id="resource-name"
                className="form"
                name="resourceName"
                fullWidth={true}
              />
            </Grid>

            <Grid item className={`${classes.descForm} label`} xs={3}>
              <InputLabel htmlFor="resource-desc">설명</InputLabel>
            </Grid>
            <Grid item className={classes.descForm} xs={9}>
              <Input
                id="resource-desc"
                className="form"
                name="resourceDesc"
                fullWidth={true}
              />
            </Grid>

            <Grid item className={`${classes.descForm} label`} xs={3}>
              <InputLabel htmlFor="api-url">호출 URL</InputLabel>
            </Grid>
            <Grid item className={classes.descForm} xs={9}>
              <Input
                id="api-url"
                className="form"
                name="apiUrl"
                fullWidth={true}
              />
            </Grid>
          </Grid>
        </Paper>
      </div>

      <div className={classes.root}>
        <div mb={4}>
          <FontAwesomeIcon icon={faSlidersH} className={classes.fontIcon} />
          파라미터 및 동작방식 설정
        </div>
      </div>
    </div>
  );
}
